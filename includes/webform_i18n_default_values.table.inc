<?php

/**
 * @file
 * Webform i18n default values table functions.
 *
 * The development sponsored by Bagler IT Inc, Dave Bagler <dave@baglerit.com>
 */

function webform_i18n_default_values_table_row_formatter(
  array &$form,
  stdClass $node
) {
  $existing_default_values_switches = array();

  foreach ($node->webform['components'] as $component) {
    // Check if the component is a supported field type.
    // If it isn't skip to the next component.
    if(!in_array(
      $component['type'],
      webform_i18n_default_values_supported_field_types()
    )) {
      continue;
    }
    
    $form['default_values'][$node->language . '-'
    . $component['cid']]['name'] = array(
      '#markup' => $component['name'] . ' (' . $component['form_key'] . ')',
    );
    $form['default_values'][$node->language . '-'
    . $component['cid']]['language'] = array(
      '#markup' => $node->language,
    );
    $form['default_values'][$node->language . '-'
    . $component['cid']]['default_value'] = array(
      '#markup' => ($component['value']? $component['value'] : '-'),
    );
    $existing_default_values_switches[$component['cid']]
      = webform_i18n_default_values_fill_language_matrix();
    $existing_default_values_switches[$component['cid']][$node->language]
      = FALSE;
  }

  $default_values = db_select('webform_i18n_default_values', 'dv')
    ->fields('dv')
    ->condition('nid', $node->nid, '=')
    ->orderBy('language', 'ASC')
    ->execute();

  while ($default_value = $default_values->fetchAssoc()) {

    $component_name = '';

    foreach ($node->webform['components'] as $component) {
      if ($component['cid'] == $default_value['cid']) {
        $component_name = $component['name']
          . ' (' . $component['form_key'] . ')';
        break;
      }
    }

    $form['default_values'][$default_value['language']
    . '-' . $default_value['cid']]['name'] = array(
      '#markup' => $component_name,
    );
    $form['default_values'][$default_value['language']
    . '-' . $default_value['cid']]['language'] = array(
      '#markup' => $default_value['language'],
    );
    $form['default_values'][$default_value['language']
    . '-' . $default_value['cid']]['default_value'] = array(
      '#markup' => $default_value['value'],
    );

    $existing_default_values_switches[$default_value['cid']]
    [$default_value['language']]
      = FALSE;
  }

  return $existing_default_values_switches;
}

/**
 * Add the 'add default value' form row to the table.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array.
 * @param array $stored_default_value_switches
 *   The two dimensional true/false array of already set default values.
 * @param \stdClass $node
 *   The node object of the webform.
 */
function webform_i18n_default_values_table_add_row(
  array &$form,
  array $form_state,
  array $existing_default_values_switches,
  stdClass $node
) {

  module_load_include(
    'inc',
    'webform_i18n_default_values',
    'includes/webform_i18n_default_values.forms'
  );

  $form['add'] = array(
    '#tree' => FALSE,
  );

  $form['add']['component'] = array(
    '#type' => 'select',
    '#options' => webform_i18n_default_values_components(
      $node,
      $existing_default_values_switches
    ),
    '#empty_option' => t('- Select Component -'),
    '#required' => TRUE,
    '#ajax' => array(
      'callback' => 'webform_i18n_default_values_entry_form_callback',
      'wrapper' => 'default_value_language_dropdown',
    ),
  );

  $form['add']['language'] = array(
    '#prefix' => '<div id="default_value_language_dropdown">',
    '#suffix' => '</div>',
    '#type' => 'select',
    '#options' => webform_i18n_default_values_available_languages(
      $form_state,
      $existing_default_values_switches
    ),
    '#empty_option' => t('- Select Language -'),
    '#required' => TRUE,
  );

  $form['add']['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
}

/**
 * Let the form know what component field types we support.
 *
 * @return array
 *   List of field types supported by this module.
 */
function webform_i18n_default_values_supported_field_types() {
  return array('textfield', 'phone', 'email', 'number');
}

/**
 * Build a two dimensional array by component then language, set to true.
 *
 * @return array
 *   The two dimensional array with each element set to true.
 */
function webform_i18n_default_values_fill_language_matrix() {
  $languages = array();
  foreach (language_list() as $language) {
    $languages[$language->language] = TRUE;
  }
  return $languages;
}
