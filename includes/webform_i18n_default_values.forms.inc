<?php

/**
 * @file
 * Webform i18n default values form functions.
 *
 * The development sponsored by Bagler IT Inc, Dave Bagler <dave@baglerit.com>
 */

/**
 * Filters the list of components.
 *
 * Filters the list of components to only the components that don't
 * have default values for all available languages.
 *
 * @param object $node
 *   The webform $node with the associated webform components.
 * @param array $default_values
 *   The list of existing default values.
 *
 * @return array
 *   The components in a keyed array to be used as select options.
 */
function webform_i18n_default_values_components($node, array $default_values) {
  // Get an array of components that don't have a default value for each
  // language.
  $available_components = webform_i18n_default_values_available_components(
    $default_values
  );
  // Format the array (cid => name) to be used as the options for the component
  // form field.
  $components = array();
  foreach ($node->webform['components'] as $component) {
    if (in_array($component['cid'], $available_components)) {
      $components[$component['cid']] = $component['name']
        . ' (' . $component['form_key'] . ')';
    }
  }
  return $components;
}

/**
 * Get the available languages for a component.
 *
 * @param array $form_state
 *   The form state array.
 * @param array $default_values
 *   The array of existing default values.
 *
 * @return array
 *   An array of available languages for example (en => English).
 */
function webform_i18n_default_values_available_languages(array $form_state,
                                                         array $default_values
) {
  if (isset($form_state['values']['component'])) {
    $available_languages = array();
    foreach (language_list() as $language) {
      if (
      $default_values[$form_state['values']['component']][$language->language]
      ) {
        $available_languages[$language->language] = $language->name;
      }
    }
    return $available_languages;
  }
  else {
    return array();
  }
}

/**
 * Get the list of available webform components.
 *
 * @param array $default_values
 *   The array of boolean values for values available.
 *
 * @return array
 *   The list of available components.
 */
function webform_i18n_default_values_available_components(
  array $default_values
) {
  $available_components = array();
  foreach ($default_values as $cid => $components) {
    foreach ($components as $language) {
      if ($language) {
        $available_components[$cid] = $cid;
      }
    }
  }
  return $available_components;
}
